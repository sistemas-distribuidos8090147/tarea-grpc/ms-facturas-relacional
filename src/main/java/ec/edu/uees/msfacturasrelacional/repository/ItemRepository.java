package ec.edu.uees.msfacturasrelacional.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import ec.edu.uees.msfacturasrelacional.entity.Item;

@Repository
public interface ItemRepository extends JpaRepository<Item, Long> {
    
}
