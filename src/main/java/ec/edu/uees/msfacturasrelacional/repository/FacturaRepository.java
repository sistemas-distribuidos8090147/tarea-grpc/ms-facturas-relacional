package ec.edu.uees.msfacturasrelacional.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import ec.edu.uees.msfacturasrelacional.entity.Factura;

@Repository
public interface FacturaRepository extends JpaRepository<Factura, Long> {
    
}
