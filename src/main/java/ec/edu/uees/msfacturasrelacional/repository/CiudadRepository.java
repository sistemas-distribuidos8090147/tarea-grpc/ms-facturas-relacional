package ec.edu.uees.msfacturasrelacional.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import ec.edu.uees.msfacturasrelacional.entity.Ciudad;


@Repository
public interface CiudadRepository extends JpaRepository<Ciudad, Long> {
    Ciudad findByNombre(String nombre);
    
}