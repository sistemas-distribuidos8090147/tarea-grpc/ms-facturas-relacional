package ec.edu.uees.msfacturasrelacional.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ec.edu.uees.msfacturasrelacional.communication.grpc.MessageSender;
import ec.edu.uees.msfacturasrelacional.dto.FacturaDTO;
import ec.edu.uees.msfacturasrelacional.entity.Factura;
import ec.edu.uees.msfacturasrelacional.entity.Item;
import ec.edu.uees.msfacturasrelacional.entity.Producto;
import ec.edu.uees.msfacturasrelacional.entity.Ciudad;
import ec.edu.uees.msfacturasrelacional.entity.Cliente;
import ec.edu.uees.msfacturasrelacional.entity.Empleado;
import ec.edu.uees.msfacturasrelacional.repository.CiudadRepository;
import ec.edu.uees.msfacturasrelacional.repository.ClienteRepository;
import ec.edu.uees.msfacturasrelacional.repository.EmpleadoRepository;
import ec.edu.uees.msfacturasrelacional.repository.FacturaRepository;
import ec.edu.uees.msfacturasrelacional.repository.ItemRepository;
import ec.edu.uees.msfacturasrelacional.repository.ProductoRepository;
import ec.edu.uees.msfacturasrelacional.service.FacturaService;
import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class FacturaServiceImpl implements FacturaService {

    @Autowired
    private MessageSender messageSender;

    private final FacturaRepository facturaRepository;
    private final ItemRepository itemRepository;
    private final ProductoRepository productoRepository;
    private final EmpleadoRepository empleadoRepository;
    private final CiudadRepository ciudadRepository;
    private final ClienteRepository clienteRepository;

    @Override
    public Factura createFactura(FacturaDTO factura) {
        Factura response = new Factura();
        Ciudad ciudad = ciudadRepository.findByNombre(factura.getCiudad());
        Empleado empleado = empleadoRepository.findByCedula(factura.getEmpleado());
        Cliente cliente = clienteRepository.findByCedula(factura.getCliente());

        if (ciudad == null) {
            ciudad = new Ciudad();
            ciudad.setNombre(factura.getCiudad());
            ciudad = ciudadRepository.save(ciudad);
        }

        if (empleado == null) {
            throw new RuntimeException("Empleado no encontrado. Debe crear el empleado primero.");
        }

        if (cliente == null) {
            throw new RuntimeException("Cliente no encontrado. Debe crear el cliente primero.");
        }

        response.setCiudadId(ciudad.getId());
        response.setEmpleadoId(empleado.getId());
        response.setClienteId(cliente.getId());
        response.setCodigo(factura.getCodigo());
        response.setFecha(factura.getFecha());
        response.setValorTotal(factura.getValorTotal());
        response.setValorSubtotal(factura.getValorSubtotal());
        response.setValorIva(factura.getValorIva());

        response = facturaRepository.save(response);

        List<ec.edu.uees.msfacturasrelacional.communication.grpc.generated.Item> itemsGrpc = 
        new ArrayList<>();

        for (Item item : factura.getItems()) {
            Producto producto = productoRepository.findByNombre(item.getProducto());
            if (producto == null) {
                producto = new Producto();
                producto.setNombre(item.getProducto());
                producto.setPrecio(item.getValorPorProducto());
                producto = productoRepository.save(producto);
            }
            ec.edu.uees.msfacturasrelacional.communication.grpc.generated.Item itemGrpc =
            ec.edu.uees.msfacturasrelacional.communication.grpc.generated.Item.newBuilder()
                .setCantidad(item.getCantidad())
                .setProducto(producto.getNombre())
                .setValorPorProducto(item.getValorPorProducto())
                .setValorUnitario(item.getValorUnitario())
                .build();
            itemsGrpc.add(itemGrpc);
            itemRepository.save(item);
        }

        
        ec.edu.uees.msfacturasrelacional.communication.grpc.generated.Factura facturaGrpc =
            ec.edu.uees.msfacturasrelacional.communication.grpc.generated.Factura.newBuilder()
                .setCiudad(factura.getCiudad())
                .setCliente(factura.getCliente())
                .setCodigo(factura.getCodigo())
                .setEmpleado(factura.getEmpleado())
                .setFecha(factura.getFecha().toString())
                .setValorIva(factura.getValorIva())
                .setValorSubTotal(factura.getValorSubtotal())
                .setValorTotal(factura.getValorTotal())
                .addAllItems(itemsGrpc)
                .build();
        messageSender.sendMessage(facturaGrpc);

        return facturaRepository.save(response);
    }
}
