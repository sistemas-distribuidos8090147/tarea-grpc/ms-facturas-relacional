package ec.edu.uees.msfacturasrelacional.service;

import ec.edu.uees.msfacturasrelacional.dto.FacturaDTO;
import ec.edu.uees.msfacturasrelacional.entity.Factura;

public interface FacturaService {
    Factura createFactura(FacturaDTO factura);
}