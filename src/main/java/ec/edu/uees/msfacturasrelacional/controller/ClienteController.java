package ec.edu.uees.msfacturasrelacional.controller;

import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import ec.edu.uees.msfacturasrelacional.entity.Cliente;
import ec.edu.uees.msfacturasrelacional.repository.ClienteRepository;
import lombok.RequiredArgsConstructor;

@RestController
@RequiredArgsConstructor
@RequestMapping("/cliente")
public class ClienteController {
    private final ClienteRepository clienteRepository;

    @PostMapping( path="/create", consumes = MediaType.APPLICATION_JSON_VALUE )
    public ResponseEntity<?> createCliente(@RequestBody Cliente cliente) {
        return ResponseEntity.ok(clienteRepository.save(cliente));
    }

    @GetMapping( path = "/getClientes" )
    public ResponseEntity<?> getClientes() {
        return ResponseEntity.ok(clienteRepository.findAll());
    }
}
