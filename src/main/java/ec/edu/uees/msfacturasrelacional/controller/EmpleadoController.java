package ec.edu.uees.msfacturasrelacional.controller;

import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import ec.edu.uees.msfacturasrelacional.entity.Empleado;
import ec.edu.uees.msfacturasrelacional.repository.EmpleadoRepository;
import lombok.RequiredArgsConstructor;

@RestController
@RequiredArgsConstructor
@RequestMapping("/empleado")
public class EmpleadoController {
    private final EmpleadoRepository empleadoRepository;

    @PostMapping( path="/create", consumes = MediaType.APPLICATION_JSON_VALUE )
    public ResponseEntity<?> createEmpleado(@RequestBody Empleado empleado) {
        return ResponseEntity.ok(empleadoRepository.save(empleado));
    }

    @GetMapping( path = "/getEmpleados" )
    public ResponseEntity<?> getEmpleados() {
        return ResponseEntity.ok(empleadoRepository.findAll());
    }
}
