package ec.edu.uees.msfacturasrelacional.controller;

import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import ec.edu.uees.msfacturasrelacional.dto.FacturaDTO;
import ec.edu.uees.msfacturasrelacional.repository.FacturaRepository;
import ec.edu.uees.msfacturasrelacional.service.FacturaService;
import lombok.RequiredArgsConstructor;

/**
 * This class represents the controller for the Factura entity, which handles HTTP requests and responses.
 * It contains methods for creating and retrieving Facturas.
 */
@RestController
@RequiredArgsConstructor
@RequestMapping("/factura")
public class FacturaController {

    private final FacturaRepository facturaRepository;
    private final FacturaService facturaService;

    /**
     * Creates a new Factura object and saves it to the database.
     * 
     * @param factura the Factura object to be created and saved
     * @return a ResponseEntity with a message indicating that the Factura was created successfully
     */
    @PostMapping( path="/create", consumes = MediaType.APPLICATION_JSON_VALUE )
    public ResponseEntity<?> createFactura(@RequestBody FacturaDTO factura) {
        return ResponseEntity.ok(facturaService.createFactura(factura));
    }

    /**
     * Retrieves all the facturas from the database.
     *
     * @return ResponseEntity with the list of facturas retrieved from the database.
     */
    @GetMapping("/getFacturas")
    public ResponseEntity<?> getFacturas() {
        return ResponseEntity.ok(facturaRepository.findAll());
    }

    /**
     * Retrieves a factura by its ID.
     *
     * @param facturaId the ID of the factura to retrieve
     * @return a ResponseEntity containing the factura if it exists, or an empty body with a 404 status code if it does not
     */
    @GetMapping("/getFactura/{facturaId}")
    public ResponseEntity<?> getFactura(Long facturaId) {
        return ResponseEntity.ok(facturaRepository.findById(facturaId));
    }
}
