package ec.edu.uees.msfacturasrelacional.entity;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import static jakarta.persistence.GenerationType.AUTO;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Item {
    @Id @GeneratedValue(strategy = AUTO)
    private Long id;
    private Long facturaId;
    private Integer cantidad;
    private String producto;
    private Double valorPorProducto;
    private Double valorUnitario;
}
