package ec.edu.uees.msfacturasrelacional.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

import lombok.Data;

@Data
@Configuration
@PropertySource("classpath:application.properties")
public class MainConfig {
    @Value("${grpc.server.host}")
    private String mainNodeHost;
    @Value("${grpc.server.port}")
    private int mainNodePort;
}
