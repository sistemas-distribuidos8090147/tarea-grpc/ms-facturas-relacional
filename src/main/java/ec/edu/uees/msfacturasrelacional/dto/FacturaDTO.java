package ec.edu.uees.msfacturasrelacional.dto;

import java.util.List;

import ec.edu.uees.msfacturasrelacional.entity.Item;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class FacturaDTO {
    private String ciudad;
    private String empleado;
    private String cliente;
    private String codigo;
    private String fecha;
    private Double valorTotal;
    private Double valorSubtotal;
    private Double valorIva;
    private List<Item> items;
}
