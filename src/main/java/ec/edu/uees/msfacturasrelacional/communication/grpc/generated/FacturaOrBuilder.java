// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: service.proto

package ec.edu.uees.msfacturasrelacional.communication.grpc.generated;

public interface FacturaOrBuilder extends
    // @@protoc_insertion_point(interface_extends:Factura)
    com.google.protobuf.MessageOrBuilder {

  /**
   * <code>string ciudad = 1;</code>
   * @return The ciudad.
   */
  java.lang.String getCiudad();
  /**
   * <code>string ciudad = 1;</code>
   * @return The bytes for ciudad.
   */
  com.google.protobuf.ByteString
      getCiudadBytes();

  /**
   * <code>string empleado = 2;</code>
   * @return The empleado.
   */
  java.lang.String getEmpleado();
  /**
   * <code>string empleado = 2;</code>
   * @return The bytes for empleado.
   */
  com.google.protobuf.ByteString
      getEmpleadoBytes();

  /**
   * <code>string cliente = 3;</code>
   * @return The cliente.
   */
  java.lang.String getCliente();
  /**
   * <code>string cliente = 3;</code>
   * @return The bytes for cliente.
   */
  com.google.protobuf.ByteString
      getClienteBytes();

  /**
   * <code>string codigo = 4;</code>
   * @return The codigo.
   */
  java.lang.String getCodigo();
  /**
   * <code>string codigo = 4;</code>
   * @return The bytes for codigo.
   */
  com.google.protobuf.ByteString
      getCodigoBytes();

  /**
   * <code>string fecha = 5;</code>
   * @return The fecha.
   */
  java.lang.String getFecha();
  /**
   * <code>string fecha = 5;</code>
   * @return The bytes for fecha.
   */
  com.google.protobuf.ByteString
      getFechaBytes();

  /**
   * <code>double valorTotal = 6;</code>
   * @return The valorTotal.
   */
  double getValorTotal();

  /**
   * <code>double valorSubTotal = 7;</code>
   * @return The valorSubTotal.
   */
  double getValorSubTotal();

  /**
   * <code>double valorIva = 8;</code>
   * @return The valorIva.
   */
  double getValorIva();

  /**
   * <code>repeated .Item items = 9;</code>
   */
  java.util.List<ec.edu.uees.msfacturasrelacional.communication.grpc.generated.Item> 
      getItemsList();
  /**
   * <code>repeated .Item items = 9;</code>
   */
  ec.edu.uees.msfacturasrelacional.communication.grpc.generated.Item getItems(int index);
  /**
   * <code>repeated .Item items = 9;</code>
   */
  int getItemsCount();
  /**
   * <code>repeated .Item items = 9;</code>
   */
  java.util.List<? extends ec.edu.uees.msfacturasrelacional.communication.grpc.generated.ItemOrBuilder> 
      getItemsOrBuilderList();
  /**
   * <code>repeated .Item items = 9;</code>
   */
  ec.edu.uees.msfacturasrelacional.communication.grpc.generated.ItemOrBuilder getItemsOrBuilder(
      int index);
}
