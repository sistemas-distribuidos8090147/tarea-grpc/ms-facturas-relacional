package ec.edu.uees.msfacturasrelacional.communication.grpc;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import ec.edu.uees.msfacturasrelacional.communication.grpc.generated.Factura;
import ec.edu.uees.msfacturasrelacional.communication.grpc.generated.FacturasServiceGrpc;
import ec.edu.uees.msfacturasrelacional.communication.grpc.generated.FacturasServiceGrpc.FacturasServiceBlockingStub;
import ec.edu.uees.msfacturasrelacional.config.MainConfig;
import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;

@Component
public class MessageSender {

    @Autowired
    private MainConfig mainConfig;

    public void sendMessage(Factura factura) {
        ManagedChannel channel = ManagedChannelBuilder.forAddress(mainConfig.getMainNodeHost(),
            mainConfig.getMainNodePort()).usePlaintext().build();
        FacturasServiceBlockingStub stub = FacturasServiceGrpc.newBlockingStub(channel);
        stub.enviarFactura(factura);
        channel.shutdown();
    }

}
