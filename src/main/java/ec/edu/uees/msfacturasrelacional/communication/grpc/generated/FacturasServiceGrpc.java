package ec.edu.uees.msfacturasrelacional.communication.grpc.generated;

import static io.grpc.stub.ClientCalls.asyncUnaryCall;
import static io.grpc.stub.ClientCalls.asyncServerStreamingCall;
import static io.grpc.stub.ClientCalls.asyncClientStreamingCall;
import static io.grpc.stub.ClientCalls.asyncBidiStreamingCall;
import static io.grpc.stub.ClientCalls.blockingUnaryCall;
import static io.grpc.stub.ClientCalls.blockingServerStreamingCall;
import static io.grpc.stub.ClientCalls.futureUnaryCall;
import static io.grpc.MethodDescriptor.generateFullMethodName;
import static io.grpc.stub.ServerCalls.asyncUnaryCall;
import static io.grpc.stub.ServerCalls.asyncServerStreamingCall;
import static io.grpc.stub.ServerCalls.asyncClientStreamingCall;
import static io.grpc.stub.ServerCalls.asyncBidiStreamingCall;
import static io.grpc.stub.ServerCalls.asyncUnimplementedUnaryCall;
import static io.grpc.stub.ServerCalls.asyncUnimplementedStreamingCall;

/**
 */
@javax.annotation.Generated(
    value = "by gRPC proto compiler (version 1.4.0)",
    comments = "Source: service.proto")
public final class FacturasServiceGrpc {

  private FacturasServiceGrpc() {}

  public static final String SERVICE_NAME = "FacturasService";

  // Static method descriptors that strictly reflect the proto.
  @io.grpc.ExperimentalApi("https://github.com/grpc/grpc-java/issues/1901")
  public static final io.grpc.MethodDescriptor<ec.edu.uees.msfacturasrelacional.communication.grpc.generated.Factura,
      com.google.protobuf.Empty> METHOD_ENVIAR_FACTURA =
      io.grpc.MethodDescriptor.<ec.edu.uees.msfacturasrelacional.communication.grpc.generated.Factura, com.google.protobuf.Empty>newBuilder()
          .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
          .setFullMethodName(generateFullMethodName(
              "FacturasService", "enviarFactura"))
          .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
              ec.edu.uees.msfacturasrelacional.communication.grpc.generated.Factura.getDefaultInstance()))
          .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
              com.google.protobuf.Empty.getDefaultInstance()))
          .build();

  /**
   * Creates a new async stub that supports all call types for the service
   */
  public static FacturasServiceStub newStub(io.grpc.Channel channel) {
    return new FacturasServiceStub(channel);
  }

  /**
   * Creates a new blocking-style stub that supports unary and streaming output calls on the service
   */
  public static FacturasServiceBlockingStub newBlockingStub(
      io.grpc.Channel channel) {
    return new FacturasServiceBlockingStub(channel);
  }

  /**
   * Creates a new ListenableFuture-style stub that supports unary calls on the service
   */
  public static FacturasServiceFutureStub newFutureStub(
      io.grpc.Channel channel) {
    return new FacturasServiceFutureStub(channel);
  }

  /**
   */
  public static abstract class FacturasServiceImplBase implements io.grpc.BindableService {

    /**
     */
    public void enviarFactura(ec.edu.uees.msfacturasrelacional.communication.grpc.generated.Factura request,
        io.grpc.stub.StreamObserver<com.google.protobuf.Empty> responseObserver) {
      asyncUnimplementedUnaryCall(METHOD_ENVIAR_FACTURA, responseObserver);
    }

    @java.lang.Override public final io.grpc.ServerServiceDefinition bindService() {
      return io.grpc.ServerServiceDefinition.builder(getServiceDescriptor())
          .addMethod(
            METHOD_ENVIAR_FACTURA,
            asyncUnaryCall(
              new MethodHandlers<
                ec.edu.uees.msfacturasrelacional.communication.grpc.generated.Factura,
                com.google.protobuf.Empty>(
                  this, METHODID_ENVIAR_FACTURA)))
          .build();
    }
  }

  /**
   */
  public static final class FacturasServiceStub extends io.grpc.stub.AbstractStub<FacturasServiceStub> {
    private FacturasServiceStub(io.grpc.Channel channel) {
      super(channel);
    }

    private FacturasServiceStub(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected FacturasServiceStub build(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      return new FacturasServiceStub(channel, callOptions);
    }

    /**
     */
    public void enviarFactura(ec.edu.uees.msfacturasrelacional.communication.grpc.generated.Factura request,
        io.grpc.stub.StreamObserver<com.google.protobuf.Empty> responseObserver) {
      asyncUnaryCall(
          getChannel().newCall(METHOD_ENVIAR_FACTURA, getCallOptions()), request, responseObserver);
    }
  }

  /**
   */
  public static final class FacturasServiceBlockingStub extends io.grpc.stub.AbstractStub<FacturasServiceBlockingStub> {
    private FacturasServiceBlockingStub(io.grpc.Channel channel) {
      super(channel);
    }

    private FacturasServiceBlockingStub(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected FacturasServiceBlockingStub build(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      return new FacturasServiceBlockingStub(channel, callOptions);
    }

    /**
     */
    public com.google.protobuf.Empty enviarFactura(ec.edu.uees.msfacturasrelacional.communication.grpc.generated.Factura request) {
      return blockingUnaryCall(
          getChannel(), METHOD_ENVIAR_FACTURA, getCallOptions(), request);
    }
  }

  /**
   */
  public static final class FacturasServiceFutureStub extends io.grpc.stub.AbstractStub<FacturasServiceFutureStub> {
    private FacturasServiceFutureStub(io.grpc.Channel channel) {
      super(channel);
    }

    private FacturasServiceFutureStub(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected FacturasServiceFutureStub build(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      return new FacturasServiceFutureStub(channel, callOptions);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<com.google.protobuf.Empty> enviarFactura(
        ec.edu.uees.msfacturasrelacional.communication.grpc.generated.Factura request) {
      return futureUnaryCall(
          getChannel().newCall(METHOD_ENVIAR_FACTURA, getCallOptions()), request);
    }
  }

  private static final int METHODID_ENVIAR_FACTURA = 0;

  private static final class MethodHandlers<Req, Resp> implements
      io.grpc.stub.ServerCalls.UnaryMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.ServerStreamingMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.ClientStreamingMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.BidiStreamingMethod<Req, Resp> {
    private final FacturasServiceImplBase serviceImpl;
    private final int methodId;

    MethodHandlers(FacturasServiceImplBase serviceImpl, int methodId) {
      this.serviceImpl = serviceImpl;
      this.methodId = methodId;
    }

    @java.lang.Override
    @java.lang.SuppressWarnings("unchecked")
    public void invoke(Req request, io.grpc.stub.StreamObserver<Resp> responseObserver) {
      switch (methodId) {
        case METHODID_ENVIAR_FACTURA:
          serviceImpl.enviarFactura((ec.edu.uees.msfacturasrelacional.communication.grpc.generated.Factura) request,
              (io.grpc.stub.StreamObserver<com.google.protobuf.Empty>) responseObserver);
          break;
        default:
          throw new AssertionError();
      }
    }

    @java.lang.Override
    @java.lang.SuppressWarnings("unchecked")
    public io.grpc.stub.StreamObserver<Req> invoke(
        io.grpc.stub.StreamObserver<Resp> responseObserver) {
      switch (methodId) {
        default:
          throw new AssertionError();
      }
    }
  }

  private static final class FacturasServiceDescriptorSupplier implements io.grpc.protobuf.ProtoFileDescriptorSupplier {
    @java.lang.Override
    public com.google.protobuf.Descriptors.FileDescriptor getFileDescriptor() {
      return ec.edu.uees.msfacturasrelacional.communication.grpc.generated.Service.getDescriptor();
    }
  }

  private static volatile io.grpc.ServiceDescriptor serviceDescriptor;

  public static io.grpc.ServiceDescriptor getServiceDescriptor() {
    io.grpc.ServiceDescriptor result = serviceDescriptor;
    if (result == null) {
      synchronized (FacturasServiceGrpc.class) {
        result = serviceDescriptor;
        if (result == null) {
          serviceDescriptor = result = io.grpc.ServiceDescriptor.newBuilder(SERVICE_NAME)
              .setSchemaDescriptor(new FacturasServiceDescriptorSupplier())
              .addMethod(METHOD_ENVIAR_FACTURA)
              .build();
        }
      }
    }
    return result;
  }
}
