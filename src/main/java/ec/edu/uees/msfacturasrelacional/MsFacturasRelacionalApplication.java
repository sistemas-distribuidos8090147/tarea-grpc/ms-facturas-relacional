package ec.edu.uees.msfacturasrelacional;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MsFacturasRelacionalApplication {

	public static void main(String[] args) {
		SpringApplication.run(MsFacturasRelacionalApplication.class, args);
	}

}
