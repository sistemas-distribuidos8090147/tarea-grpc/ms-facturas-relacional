# Indicaciones Generales
Este microservicio funciona con una base de datos PostgreSQL, por lo que si no se encuentra correctamente configurado no va a ejecutarse.

## Para ejecutar el microservicio
1. Verificar que el archivo `application.properties` se encuentre en el directorio `src/main/resources`.
2. Ejecutar el comando `./mvnw clean install`
3. Ejecutar el comando `./mvnw spring-boot:run`.

> **Nota:** La base de datos PostgreSQL se levantó usando [Render](https://render.com), que al ser un servicio gratuito, en cualquier momento podría presentar inconvenientes o dejar de funcionar. Por ende, si no funciona la conexión con la base de datos, se puede levantar una localmente usando una imagen de docker o directamente algún motor de base de datos relacional y conectarlo a la aplicación.


## Para probar la aplicación
- El microservicio está levantado en el puerto 8080.
- A continuación se muestran ejemplos de requests con datos previamente insertados en tiempo de desarrollo:
    1. Para consultar las facturas existentes en la base de datos (GET Request): `http://localhost:8080/factura/getFacturas`
    2. Para crear nuevas facturas (POST Request):
        - Request URL: `http://localhost:8080/factura/create`
        - Request Body:
        ```json
        {
            "ciudad": "Quito",
            "empleado": "0987654321",
            "cliente": "0987654321",
            "codigo": "Codigo",
            "fecha": "21-11-2023",
            "valorTotal": 10,
            "valorSubtotal": 8,
            "valorIva": 2,
            "items": [
                {
                    "cantidad": 1,
                    "producto": "Prueba Producto 1",
                    "valorPorProducto": 4,
                    "valorUnitario": 4
                },
                {
                    "cantidad": 1,
                    "producto": "Prueba Producto 2",
                    "valorPorProducto": 4,
                    "valorUnitario": 4
                }
            ]
        }
        ```
    3. No se podrán crear facturas si la cédula del cliente o empleado que se envíe en el request no existe previamente en la base de datos. A continuación se detallan ejemplos de request para crear empleados y clientes en caso de que requerirse.
    4. Para crear empleados (POST Request):
        - Request URL: `http://localhost:8080/empleado/create`
        - Request Body:
        ```json
        {
            "cedula":"0987654321",
            "nombres":"NOMBRES EMPLEADO 1",
            "apellidos":"APELLIDOS EMPLEADO 1"
        }
        ```
    5. Para crear clientes (POST Request):
        - Request URL: `http://localhost:8080/cliente/create`
        - Request Body:
        ```json
        {
            "cedula":"0987654321",
            "nombres":"Prueba cliente nombres 1",
            "apellidos":"Prueba cliente apellidos 1"
        }
        ```
> **Nota:** En la base de datos de [Render](https://render.com) hay un registro de empleados y clientes creado con cédula `0987654321`.